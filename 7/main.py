with open("out.txt", "r") as f:
    lines = f.readlines()
    n = len(lines)


k = 0
for i in lines:
    g = list(map(int, i.split()))
    s = bin(g[0]).count("1") + bin(g[1]).count("1") + bin(g[2]).count("1") + bin(g[3]).count("1")
    if s % 2 == g[-1]:
        k += 1


print(str(100 - k) + '%')
