def hash(_string):
    arr = []
    for i in _string:
        arr.append(ord(i))
        print(f"{i}" + " = " + (bin(ord(i))))
    answer = ""
    for i in range(7):
        sum = 0
        for j in arr:
            sum += ((j >> (6 - i)) % 2)
        sum %= 2
        answer += str(sum)
    return answer


_str = 'asdqwerty'

print('hash =',hash(_str))
