#include<iostream>
#include<string>


class NDArray{
    private: 
        int* arr;
        int shape[2];
        int size;

    public:
        NDArray(int y, int x) {
            shape[0] = y;
            shape[1] = x;
            size = x * y;
            arr = new int[size];
        };
        NDArray(int x, int y, int fill){
            shape[0] = x;
            shape[1] = y;
            size = _prod();
            arr = new int[size];
            to_fill(fill);
        };

        int _prod();
        void to_fill(int fill);
        void _null();
        void ones();
        void zeroes();
        void rand_gen(int min, int max);
        int sum();
        NDArray transpos();
        void matmul(NDArray &other);
        std::string display();
        int get_index(int x, int y);
};
std::ostream& operator<<(std::ostream &os, NDArray arr) {
    os << arr.display();
    return os;
}
NDArray operator+(const NDArray& val1, const NDArray& val2){
    return val1+val2;
};
NDArray operator-(const NDArray& val1, const NDArray& val2){
    return val1-val2;
};
NDArray operator/(const NDArray& val1, const NDArray& val2){
    return val1/val2;
};
NDArray operator*(const NDArray& val1, const NDArray& val2){
    return val1*val2;
};
int NDArray::_prod(){
    size = shape[0] * shape[1];
    return size;
}
void NDArray::to_fill(int fill){
    for (int i =0; i< size; i++){
        arr[i] = fill;
    }
}
void NDArray::_null(){
    arr = new int[size];
}
void NDArray::zeroes(){
    to_fill(1);
}
void NDArray::ones(){
    to_fill(0);
}
void NDArray::rand_gen(int r_min, int r_max){
    for (int i = 0; i < size; i++){
        arr[i] = rand() % r_max + r_min;
    }
}
std::string NDArray::display(){
    std::string res;
    for (int i =0 ; i <shape[0]; i++){
        for (int j = 0; j<shape[1]; j++){
            res += std::to_string(arr[get_index(i,j)]) + " ";
        }
        res += '\n';
    }
    return res;
}
int NDArray::sum(){
    int sum = 0;
    for (int i =0;  i < size; i++){
        sum += arr[i];
    }
    return sum;
}
 NDArray NDArray::transpos(){
    NDArray tranM(shape[1], shape[0]);
    for (int i = 0; i <shape[1]; i++){
        for (int j =0; j<shape[0]; i++){
            tranM.arr[tranM.get_index(i,j)] = arr[get_index(j,i)];
        }
    }
    return tranM;
}
// void NDArray::matmul(NDArray &other){
//     NDArray multyM(shape[0], other.shape[1]);
//     for (int i =0; i < shape[0]; i++){
//         for (int j = 0; j< shape[1]; j++){
//             multyM.arr[get_index(i,j)] = 

//         }
//     }
// }


int NDArray::get_index(int x, int y){
    return(x*shape[0] + y);
}

int main(){


    return 0;
}