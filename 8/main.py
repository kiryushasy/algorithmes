import random
import time
import os

def CRC(s):
    h = 0
    for i in range(len(s)):
        ki = ord(s[i])
        highorder = h & 0xf8000000
        h = h << 5
        h = h ^ (highorder >> 27)
        h = h ^ ki
    return h

def PJW(s):
    h = 0
    for i in range(len(s)):
        ki = ord(s[i])
        h = (h << 4) + ki
        g = h & 0xf0000000
        if g != 0:
            h = h ^ (g >> 24)
            h = h ^ g
    return h

def BUZ(s):
    h = 0
    for i in range(len(s)):
        ki = int(ord(s[i]))
        highorder = h & 0x80000000
        h = h << 1
        h = h ^ (highorder >> 31)
        h = h ^ R(ki)
    return h

def R(c):
    random.seed(1)
    rand_ch = c * random.randint(0,1000)
    return rand_ch

def read_files(rep):
    list = []
    files = os.listdir(rep)
    for name in files:
        with open(rep + '/' + name, "r") as f:
            file = f.read()
            list.append(file)
    return list

def fd(func, list):
    start = time.time()
    dubles = set(map(func, list))
    time_e = time.time() - start
    print("Время выполнения: ", time_e)
    print("Дубликатов: ", len(list) - len(dubles))


files = read_files("./out")
fd(CRC, files)
fd(PJW, files)
fd(BUZ, files)
