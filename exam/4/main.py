import matplotlib.pyplot as plt
import networkx as nx
import json


def draw_graph(graph):
    pos = nx.spring_layout(graph)

    plt.figure(figsize=(10,10))
    nx.draw_networkx_nodes(graph, pos)
    nx.draw_networkx_edges(graph, pos, edges)
    nx.draw_networkx_labels(graph, pos, font_size=5, labels=nameStantions)
    labels = nx.get_edge_attributes(graph, "weight")
    nx.draw_networkx_edge_labels(graph, pos, font_size=5, edge_labels=labels)


with open ("railways.json", "r")as f:
    railways = json.load(f)
with open("routes.json", "r") as f2:
    routes = json.load(f2)
nodes = []
stantions = []
edges = []

for i in range(len(railways['data'])):
    nodes.append(railways['data'][i]['code'])
    stantions.append(railways['data'][i]['name'])

nameStantions = dict(zip(nodes,stantions))
# print(nameStantions)

for i in range(len(routes)):
    edges.append(tuple(routes[i].values()))


# print(nodes)
# print(stantions)
# print(edges)

graph = nx.Graph()
graph.add_nodes_from(nodes)
graph.add_weighted_edges_from(edges)
draw_graph(graph)
plt.show()
