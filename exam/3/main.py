def dfc(graph, point):
    while point is not None:
        if graph[point]['left'] is not None:
            if not graph[graph[point]['left']]['visited']:
                point = graph[point]['left']
                continue
        if graph[point]['right'] is not None:
            if not graph[graph[point]['right']]['visited']:
                point = graph[point]['right']
                continue
        print(point, end=' ')
        graph[point]['visited'] = True
        point = graph[point]['parent']

graph = {
    0: {'left': 1, 'right': 2, 'parent': None, 'visited': False},
    1: {'left': 3, 'right': 4, 'parent': 0, 'visited': False},
    2: {'left': 5, 'right': 6, 'parent': 0, 'visited': False},
    3: {'left': None, 'right': None, 'parent': 1, 'visited': False},
    4: {'left': None, 'right': None, 'parent': 1, 'visited': False},
    5: {'left': None, 'right': None, 'parent': 2, 'visited': False},
    6: {'left': None, 'right': None, 'parent': 2, 'visited': False}
}

dfc(graph, 0)