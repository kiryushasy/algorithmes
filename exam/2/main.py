def odd(s):
    return s.count('1') % 2

def hash(s):
    result = []

    for i in s:
        bin_s = bin(ord(i))[2:]

    for i in range(len(bin_s)):
        bin_s = '0' + bin_s[i]
        temp = []

        for i in range(0, 8, 2):
            temp.append(bin_s)
        result.append(odd(''.join(temp)))
        return int(''.join(map(str, result[-16:])), 2)
hash_buf = []
with open ("words.txt", "r") as f:
    for i in f.read().split():
        hashed = hash(i)
        hash_buf.append(hashed)
print(len(hash_buf) - len(set(hash_buf)))
