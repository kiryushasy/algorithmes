import matplotlib.pyplot as plt


x = []
y =[]


with open ("out.dat", "r") as f:
    for lines in f.readlines():
        x.append(int(lines.split()[0]))
        y.append(float(lines.split()[1]))
plt.plot(x,y)
plt.show()
