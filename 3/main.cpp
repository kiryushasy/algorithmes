#include <iostream>
#include <vector>
#include <cmath>

int main()
{
  std::vector<float> vec;
  int s;
  for (int i=0; i<=5; i++) {
    s = std::ceil(std::pow(10, i));
    if (s == 10){
      s = 14;
    }
    vec.clear();
    for (int j=0; j<=s; j++) {
      vec.push_back(j);
    }
    std::cout << s << " " << sizeof(std::vector<float>) + sizeof(float) * vec.capacity() << std::endl;
  }
  std::cout << std::endl;
  return 0;
}